﻿using BasicApiStarterKit.Context;
using BasicApiStarterKit.Repositories;
using BasicApiStarterKit.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit
{
    public static class DependencyProfile
    {
        public static void DependencyLoad(this IServiceCollection services)
        {
            // context
            services.AddSingleton<IDbContext, ApplicationDbContext>();

            // repository and services for entities
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductService, ProductService>();
        }
    }
}
