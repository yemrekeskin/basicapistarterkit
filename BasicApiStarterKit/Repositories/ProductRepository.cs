﻿using BasicApiStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit.Repositories
{
    public interface IProductRepository
        : IRepository<Product>
    {

    }

    public class ProductRepository
        : BaseRepository<Product>, IProductRepository
    {

    }
}
