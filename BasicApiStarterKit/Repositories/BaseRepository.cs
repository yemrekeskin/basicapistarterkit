﻿using BasicApiStarterKit.Context;
using BasicApiStarterKit.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BasicApiStarterKit.Repositories
{
    public abstract class BaseRepository<TModel>
        : IRepository<TModel>
        where TModel : BaseModel
    {
        private readonly IDbContext context;

        public BaseRepository(IDbContext context)
        {
            this.context = context;
        }

        public BaseRepository()
        {
            this.context = new ApplicationDbContext();
        }


        public virtual void Add(TModel entity)
        {
            var table = context.Set<TModel>();
            table.Add(entity);
            this.SaveChanges();
        }

        public virtual void AddBatch(ICollection<TModel> models)
        {
            var table = context.Set<TModel>();
            foreach (var entity in models)
            {
                table.Add(entity);
            }
            this.SaveChanges();
        }

        public virtual IQueryable<TModel> List(params Expression<Func<TModel, object>>[] includeProperties)
        {
            IQueryable<TModel> entities = context.Set<TModel>();
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    entities = entities.Include(includeProperty);
                }
            }
            return entities;
        }

        public virtual IQueryable<TModel> List(Expression<Func<TModel, bool>> predicate)
        {
            var table = this.List();
            var result = table.Where(predicate);
            return result;
        }

        public virtual IQueryable<TModel> List(Expression<Func<TModel, bool>> predicate, params Expression<Func<TModel, object>>[] includeProperties)
        {
            var table = this.List(includeProperties);
            var result = table.Where(predicate);
            return result;
        }

        public virtual TModel Get(long id, params Expression<Func<TModel, object>>[] includeProperties)
        {
            var table = this.List(includeProperties);
            var result = table.SingleOrDefault(d => d.Id == id);
            return result;
        }

        public virtual TModel Remove(long id)
        {
            TModel entity = this.Get(id);
            entity.IsDeleted = true;
            this.Update(entity);

            return entity;
        }

        public virtual void Remove(TModel model)
        {
            model.IsDeleted = true;
            this.Update(model);
        }

        public virtual void RemoveBatch(ICollection<TModel> models)
        {
            foreach (var entity in models)
            {
                entity.IsDeleted = true;
            }
            this.context.SaveChanges();
        }

        public virtual void Update(TModel model)
        {
            this.SaveChanges();
        }

        public IQueryable<TModel> List(IEnumerable<long> ids)
        {
            var table = this.List().Where(d => ids.Contains(d.Id));
            return table;
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }
    }
}
