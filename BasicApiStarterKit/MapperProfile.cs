﻿using AutoMapper;
using BasicApiStarterKit.Models;
using BasicApiStarterKit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit
{
    public class MapperProfile
        : Profile
    {
        public MapperProfile()
        {
            CreateMap<ProductForCreate, Product>();
            CreateMap<ProductForUpdate, Product>();
            CreateMap<ProductForGet, Product>();
            CreateMap<ProductForList, Product>();

            CreateMap<Product, ProductForCreate>();
            CreateMap<Product, ProductForUpdate>();
            CreateMap<Product, ProductForGet>();
            CreateMap<Product, ProductForList>();
        }
    }
}
