﻿using BasicApiStarterKit.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit.Models
{
    public abstract class BaseModel
        : IModel
    {
        public string UniqueKey { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public bool IsDeleted { get; set; }

        public ActivePassive ActivePassive { get; set; }
        public int Id { get; set; }

        public BaseModel()
        {
            this.UniqueKey = Guid.NewGuid().ToString();
        }
    }
}
