﻿using BasicApiStarterKit.Common;
using BasicApiStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit.ViewModels
{
    public class BaseViewModel
    {
        public string UniqueKey { get; set; }

        public bool IsDeleted { get; set; }

        public ActivePassive ActivePassive { get; set; }
        public int Id { get; set; }
    }
}
