﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit.Common
{
    public enum ActivePassive
    {
        Active = 1,
        Passive = 0
    }
}
