﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BasicApiStarterKit.ModelBinder;
using BasicApiStarterKit.Models;
using BasicApiStarterKit.Services;
using BasicApiStarterKit.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicApiStarterKit.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/bulk/products")]
    [ApiController]
    public class ProductBulkController
        : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IProductService productService;

        public ProductBulkController(
            IMapper mapper,
            IProductService productService)
        {
            this.mapper = mapper;
            this.productService = productService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<ProductForList> list)
        {
            if (list == null)
            {
                return BadRequest();
            }

            var productList = mapper.Map<IEnumerable<Product>>(list);

            foreach (var product in productList)
            {
                productService.Add(product);
            }

            var productCollectionToReturn = mapper.Map<IEnumerable<ProductForList>>(productList);
            var idsAsString = string.Join(",",
                productCollectionToReturn.Select(a => a.Id));

            return CreatedAtRoute("GetProductCollection",
                new { ids = idsAsString },
                productCollectionToReturn);
        }

        [HttpGet("({ids})", Name = "GetProductCollection")]
        public IActionResult GetCollection(
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<long> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }
            var list = productService.List(ids);

            if (ids.Count() != list.Count())
            {
                return NotFound();
            }

            var productToReturn = Mapper.Map<IEnumerable<ProductForGet>>(list);
            return Ok(productToReturn);
        }
    }
}
