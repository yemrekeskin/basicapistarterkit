﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BasicApiStarterKit.Models;
using BasicApiStarterKit.Services;
using BasicApiStarterKit.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BasicApiStarterKit.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/products")]
    [ApiController]
    public class ProductController 
        : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IProductService productService;

        public ProductController(
            IMapper mapper,
            IProductService productService)
        {
            this.mapper = mapper;
            this.productService = productService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var list = productService.List(); 
            var result = mapper.Map<IEnumerable<ProductForList>>(list);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult Get(long id)
        {
            var product = productService.Get(id);
            if (product == null)
            {
                return NotFound();
            }
            var result = mapper.Map<ProductForGet>(product);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProductForCreate productForCreate)
        {
            if (productForCreate == null)
            {
                return BadRequest();
            }

            var product = mapper.Map<Product>(productForCreate);
            productService.Add(product);

            if (product.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }
            var result = mapper.Map<ProductForCreate>(product);
            return CreatedAtRoute("GetProduct", new { result.Id }, result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var product = productService.Get(id);
            if (product == null)
            {
                return NotFound();
            }

            productService.Remove(product);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] ProductForUpdate productForUpdate)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (productForUpdate == null)
            {
                return BadRequest();
            }

            var product = productService.Get(id);
            if (product == null)
            {
                return NotFound();
            }

            try
            {
                productForUpdate.Id = Convert.ToInt32(id);
                var result = mapper.Map<Product>(productForUpdate);
                productService.Update(result);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
