﻿using BasicApiStarterKit.Models;
using BasicApiStarterKit.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicApiStarterKit.Services
{
    public interface IProductService
        : IService<Product>
    {

    }

    public class ProductService
        : BaseService<Product>, IProductService
    {
        public ProductService(IProductRepository repo)
            : base(repo)
        {

        }
    }
}
