**BasicAPIStarterKit**

* [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=YunusEmreKeskin.BasicApiStarterKit)
* Basic API StarterKit for web and mobile applications
* Basic API StarterKit is startup project for creating asp.net core web application.The project template consist of a single project. It provides basic needs for basic api management for web and mobile application,

**API Management Project version 1.0**

*  API versioning
*  API help page
*  API simple crud operations
*  API bulk insert